module.exports = {
  name: 'dependencies',
  displayName: 'dependencies',
  collectCoverage: true,
  coverageDirectory: '<rootDir>/coverage/dependencies',
  coverageReporters: ['lcov', 'html'],
  testMatch: ['<rootDir>/test/dependencies/*.spec.js']
};
