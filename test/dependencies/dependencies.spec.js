/* eslint-disable no-unused-expressions */

const util = require('util');
const depcheck = require('depcheck');
const { expect } = require('chai');

const checkUnused = util.promisify(depcheck);

describe('Test Dependencies', () => {
  it('Check unused', async () => {
    try {
      await checkUnused(process.cwd(), {
        ignoreMatches: [
          'lint-staged',
          'raml2html'
        ]
      });
    } catch (error) {
      expect(error.dependencies).to.be.empty;
      expect(error.devDependencies).to.be.empty;
    }
  });
});
