const express = require('express');
const path = require('path');

module.exports = () => {
  const router = express.Router();
  router.use('/doc', express.static(path.join(__dirname, '../../doc')));
  return router;
};
