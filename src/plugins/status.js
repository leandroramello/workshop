const { Router } = require('express');
const { ARTIFACTDETAILS, APPNAME } = require('../config');

module.exports = () => {
  const router = Router();
  router.get('/', (req, res) => res.send(`${APPNAME} has been started!`));
  router.get('/version', (req, res) => res.json(ARTIFACTDETAILS));
  return router;
};
