const { createContainer, asValue, asClass } = require('awilix');
const { pipe, toPairs, map, fromPairs } = require('ramda');
const { camelCase } = require('change-case');

const services = require('../app/services');
const models = require('../app/models');

const container = createContainer();

container.register(
  pipe(
    toPairs,
    map(([key, value]) => [camelCase(key), asClass(value).scoped()]),
    fromPairs
  )(services)
);

container.register(
  pipe(
    toPairs,
    map(([key, value]) => [key, asValue(value)]),
    fromPairs
  )(models)
);
module.exports = () => (req, res, next) => {
  req.scope = container.createScope();
  next();
};
