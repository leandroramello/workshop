const bodyParser = require('./bodyParser');
const status = require('./status');
const doc = require('./doc');
const scopeInjection = require('./scopeInjection');
const queryInt = require('./queryInt');

module.exports = [bodyParser(), status(), doc(), scopeInjection(), queryInt()];
