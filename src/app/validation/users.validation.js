const Joi = require('joi');

module.exports = {
  get: {
    params: {
      id: Joi.string()
        .regex(/^[0-9a-fA-F]{24}$/, 'valid id')
    }
  },
  search: {},
  create: {
    body: Joi.object()
      .keys({
        _id: Joi.forbidden(),
        name: Joi.string(),
      })
      .options({ allowUnknown: false })
  },
  update: {
    params: Joi.object()
      .keys({
        id: Joi.string()
          .regex(/^[0-9a-fA-F]{24}$/, 'valid id')
          .required()
      })
      .options({ allowUnknown: false }),
    body: Joi.object()
      .keys({
        _id: Joi.forbidden(),
        name: Joi.string(),
      })
      .options({ allowUnknown: false })
  },
  delete: {
    params: Joi.object()
      .keys({
        id: Joi.string()
          .regex(/^[0-9a-fA-F]{24}$/, 'valid id')
          .required()
      })
      .options({ allowUnknown: false }),
    body: Joi.object()
      .keys({})
      .options({ allowUnknown: false })
  }
};
