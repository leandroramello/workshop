const { deepParseJson } = require('deep-parse-json');
const {
  EntityExistsUnprocessableEntityError,
  EntityNotFoundError
} = require('../common/errors');
const { objectToDotNotation } = require('../../core/utils');
const { search } = require('../../config');

module.exports = class UsersService {
  constructor(deps) {
    this.Model = deps.UsersModel;
  }

  async get(id) {
    const modelObject = await this.Model;
    const obtained = await modelObject.findOne({ _id: id });
    if (!obtained) {
      throw new EntityNotFoundError(id);
    }
    return obtained;
  }

  async search({
                 select,
                 populate,
                 page,
                 limit,
                 sortField,
                 sortDirection,
                 ...queryParams
               }) {
    const modelObject = await this.Model;
    const options = {
      select: select || '',
      populate: populate || [],
      page: page || search.pageOptions.page,
      limit: limit || search.pageOptions.limit,
      sort: sortField
        ? { [sortField]: sortDirection || search.pageOptions.sort.key }
        : search.pageOptions.sort
    };
    const queryObject = deepParseJson({ ...queryParams });
    return modelObject.paginate({ ...queryObject }, options);
  }

  async create(nodeData) {
    let nodeCreated = null;
    try {
      const modelObject = await this.Model;
      const data = {
        ...nodeData
      };

      nodeCreated = await modelObject.create(data);
    } catch (error) {
      if (error.name === 'MongoError' && error.code === 11000) {
        // ID already exists
        throw new EntityExistsUnprocessableEntityError(nodeData);
      }
      throw error;
    }

    return nodeCreated;
  }

  async update(id, nodeData) {
    let updated = false;
    try {
      const modelObject = await this.Model;
      const data = {
        ...nodeData
      };

      updated = await modelObject.findOneAndUpdate(
        { _id: id },
        { $set: objectToDotNotation(data) },
        { new: true }
      );
    } catch (error) {
      if (error.name === 'MongoError' && error.code === 11000) {
        // ID already exists
        throw new EntityExistsUnprocessableEntityError(nodeData);
      }
      throw error;
    }

    if (!updated) {
      throw new EntityNotFoundError(id);
    }

    return updated;
  }

  async delete(id) {
    const modelObject = await this.Model;
    const deleted =
      (await modelObject.deleteOne({
        _id: id
      })).n > 0;
    return { id, deleted };
  }
};
