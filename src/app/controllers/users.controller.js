const Module = {};

Module.get = async (req, res) => {
  const service = req.scope.resolve('usersService');
  const result = await service.get(req.params.id);
  res.json(result);
};
Module.search = async (req, res) => {
  const service = req.scope.resolve('usersService');
  const result = await service.search(req.query);
  res.json(result);
};
Module.create = async (req, res) => {
  const service = req.scope.resolve('usersService');
  const asset = await service.create(req.body);
  res.json(asset);
};
Module.update = async (req, res) => {
  const service = req.scope.resolve('usersService');
  const asset = await service.update(req.params.id, req.body);
  res.json(asset);
};
Module.delete = async (req, res) => {
  const service = req.scope.resolve('usersService');
  const result = await service.delete(req.params.id);
  res.json(result);
};

module.exports = Module;
