const { Router } = require('express');
const { asPromise } = require('../../core/utils');
const ctrl = require('../controllers/users.controller');

const validation = require('../validation');
const validationMiddleware = require('../../core/errors/validation/validationMiddleware');

const router = Router();

router.get(
  `/:id`,
  validationMiddleware(validation.users.get),
  asPromise(ctrl.get)
);
router.get(
  '/',
  validationMiddleware(validation.users.search),
  asPromise(ctrl.search)
);

router.post(
  `/`,
  validationMiddleware(validation.users.create),
  asPromise(ctrl.create)
);
router.put(
  `/:id`,
  validationMiddleware(validation.users.update),
  asPromise(ctrl.update)
);
router.delete(
  `/:id`,
  validationMiddleware(validation.users.delete),
  asPromise(ctrl.delete)
);

module.exports = router;
