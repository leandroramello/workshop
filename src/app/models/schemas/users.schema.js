const mongoosePaginate = require('mongoose-paginate');
const { Schema } = require('mongoose');

const UsersDefinition = {
  name: {
    type: String
  }
};

const UsersSchema = new Schema(UsersDefinition, {
  versionKey: false,
  minimize: false,
  strict: true
});

UsersSchema.plugin(mongoosePaginate);

module.exports = UsersSchema;
