/* eslint-disable no-param-reassign */
const { ValidationError } = require('express-validation');
const { FUNCTIONALGROUP } = require('../../../config');

exports.BadGatewayError = class BadGatewayError extends ValidationError {
  constructor(data) {
    const errors = [
      {
        type: 'BadGatewayError',
        data,
        description: `Comunication error processing ${JSON.stringify(data)}`
      }
    ];
    const options = {
      status: 502,
      statusText: `${FUNCTIONALGROUP} - Bad Gateway`
    };

    super(errors, options);
  }
};

exports.EntityNotFoundError = class EntityNotFoundError extends ValidationError {
  constructor(data) {
    const errors = [
      {
        type: 'EntityNotFoundError',
        data,
        description: `Entity ${JSON.stringify(data)} not found`
      }
    ];
    const options = {
      status: 404,
      statusText: `${FUNCTIONALGROUP} - Not Found`
    };

    super(errors, options);
  }
};