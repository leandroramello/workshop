const core = require('./core');

if (core.rest) {
  core.rest.start(r => {
    console.log(`Http server started on port ${r.port}`);
  });
}

process.once('SIGTERM', () => {
  if (core.rest) {
    core.rest.stop(() => {
      console.log(`Http server stopped`);
    });
  }
});
