const { UsersRouter } = require('../app/routes');

module.exports = {
  '/users': UsersRouter
};
