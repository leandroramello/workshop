const artifactDetails = require('../../ArtifactDetails.json');

module.exports = {
  ARTIFACTDETAILS: artifactDetails,
  FUNCTIONALGROUP: process.env.FUNCTIONALGROUP || 'UsersManager',
  APPNAME: artifactDetails.name,
  APPVERSION: artifactDetails.version,
  PORT: process.env.SERVER_PORT || 9100,
  MONGODB_URL: process.env.MONGODB_URL || 'mongodb+srv://workshop:workshop@cluster0-qeqe3.mongodb.net/test?retryWrites=true&w=majority',
  search: {
    pageOptions: {
      limit: 6,
      page: 1,
      sort: { key: 1 }
    }
  }
};
