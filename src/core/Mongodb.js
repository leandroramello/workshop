const mongoose = require('mongoose');
const { MONGODB_URL } = require('../config');

mongoose.set('bufferCommands', false);
mongoose.Promise = global.Promise;

module.exports = {
  async start() {
    try {
      await mongoose.connect(MONGODB_URL, {
        useNewUrlParser: true,
        useCreateIndex: true
      });
      console.info('Database connected succesfully');
    } catch (error) {
      console.error(error);
    }
  },

  stop() {
    mongoose.connection.close();
  }
};
