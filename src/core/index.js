const server = require('./Server');

const errorHandler = require('./errors/validation/errorsHandler');
const routes = require('../config/routes');
const plugins = require('../plugins');
const db = require('./Mongodb');
const { PORT } = require('../config');

const rest = server.create({ plugins, routes, errorHandler, db, port: PORT });

module.exports = { rest };
