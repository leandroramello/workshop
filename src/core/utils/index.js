const { identity } = require('ramda');

exports.asPromise = (fn, formatError = identity) => async (req, res, next) => {
  try {
    await fn(req, res, next);
  } catch (error) {
    next(formatError(error));
  }
};

exports.objectToDotNotation = args => {
  const setObject = {};
  Object.keys(args).forEach(key => {
    if (typeof args[key] === 'object') {
      Object.keys(args[key]).forEach(subkey => {
        setObject[`${key}.${subkey}`] = args[key][subkey];
      });
    } else {
      setObject[key] = args[key];
    }
  });
  return setObject;
};
