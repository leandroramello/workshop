/* eslint-disable no-param-reassign, no-unused-vars */
const { ValidationError } = require('express-validation');
const schemaValidator = require('./validation/jsonSchemaValidator.js');
const errorSchema = require('./validation/schemas/error.schema.json');
const { FUNCTIONALGROUP } = require('../../config');

exports.SchemaValidationBadRequestError = class SchemaValidationBadRequestError extends ValidationError {
  constructor(data) {
    data.forEach(item => {
      item.type = 'SchemaValidationBadRequestError';
      item.description = item.message || item.messages.join(', ') || '';
    });

    const options = {
      status: 400,
      statusText: `${FUNCTIONALGROUP} - Bad Request`
    };

    super(data, options);
  }
};

exports.InternalServerError = class InternalServerError extends ValidationError {
  constructor(data) {
    const errors = [
      {
        type: 'InternalServerError',
        data,
        description: `InternalServerError`
      }
    ];
    const options = {
      status: 500,
      statusText: `${FUNCTIONALGROUP} - InternalServerError`
    };

    super(errors, options);
  }
};
