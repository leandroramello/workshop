const validate = require('express-validation');
const { ValidationError } = require('express-validation');
const { asPromise } = require('../../utils');
const { SchemaValidationBadRequestError } = require('../');

module.exports = schema =>
  asPromise(async (req, res, next) =>
    validate(schema)(req, res, result => {
      if (result instanceof ValidationError) {
        throw new SchemaValidationBadRequestError(result.errors);
      }
      next();
    })
  );
