/* eslint-disable no-param-reassign, no-unused-vars */
const schemaValidator = require('./jsonSchemaValidator.js');
const errorSchema = require('./schemas/error.schema.json');
const { FUNCTIONALGROUP } = require('../../../config');
const { InternalServerError } = require('../');

module.exports = async (error, req, res, next) => {
  let errorOut = error;
  if (
    error.error &&
    error.error.statusText &&
    error.error.statusText.startsWith(FUNCTIONALGROUP)
  ) {
    errorOut = error.error;
  }
  try {
    await schemaValidator.validate(errorOut, errorSchema);
    return res.status(errorOut.status).json(errorOut);
  } catch (e) {
    return res.status(500).json(new InternalServerError(errorOut));
  }
};
