/* eslint-disable no-param-reassign */
const Ajv = require('ajv');
const { SchemaValidationBadRequestError } = require('../');

const ajv = new Ajv({ allErrors: true, removeAdditional: false });

module.exports = class JsonSchemaValidator {
  static validate(data, schema) {
    return new Promise(resolve => {
      const valid = ajv.validate(schema, data);
      if (valid) {
        resolve({ valid, data });
      } else {
        throw new SchemaValidationBadRequestError(ajv.errors);
      }
    });
  }
};
