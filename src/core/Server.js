const express = require('express');

module.exports = class Server {
  static create({ plugins, routes, errorHandler, db, port }) {
    const server = new Server(port);

    // Register plugins
    if (plugins) {
      server.register(plugins);
    }
    // Register routes
    if (routes) {
      server.registerRoutes(routes);
    }
    // Register core error handler
    if (errorHandler) {
      server.register(errorHandler);
    }
    // Register db
    if (db) {
      server.registerDb(db);
    }

    return server;
  }

  constructor(port) {
    this.app = express();
    this.port = port;
    this.db = null;
    this.events = {};
  }

  register(element) {
    if (Array.isArray(element)) {
      this.app.use(...element);
    } else if (element) {
      this.app.use(element);
    }
  }

  registerRoutes(_routes = {}) {
    Object.entries(_routes).forEach(([path, router]) => {
      if (Array.isArray(router)) {
        this.app.use(path, ...router);
      } else {
        this.app.use(path, router);
      }
    });
    return this;
  }

  registerDb(db) {
    this.db = db;
    return this;
  }

  async start(callback, error) {
    try {
      if (this.db) {
        await this.db.start();
      }
      this.events.started = this.app.listen(this.port);
      if (callback) {
        callback({ port: this.port });
      }
    } catch (e) {
      if (error) {
        error(e);
      }
    }
  }

  stop(callback, error) {
    try {
      if (this.events.started) {
        this.events.started.close();
      }
      if (callback) {
        callback();
      }
    } catch (e) {
      if (error) {
        error(e);
      }
    }
  }
};
