/* eslint-disable import/no-extraneous-dependencies */
const gulp = require('gulp');
const del = require('del');
const shell = require('gulp-shell');

gulp.task('clean', () => del(['doc']));

gulp.task('init-doc', () =>
  gulp.src('.', { read: false }).pipe(shell(['mkdir doc']))
);

gulp.task('generate-doc', () =>
  gulp
    .src('apidef/api.raml', { read: false })
    .pipe(shell(['raml2html -i apidef/api.raml -o doc/index.html']))
);

gulp.task(
  'doc',
  gulp.series('clean', 'init-doc', 'generate-doc', done => {
    done();
  })
);
